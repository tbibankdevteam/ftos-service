﻿using FTOS_GetUsersEnrollment.Data;
using FTOS_GetUsersEnrollment.Models.Models;
using FTOS_GetUsersEnrollment.Web.Helper.Entities;
using FTOS_GetUsersEnrollment.Web.Helper.Entities.HelpDesk;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Timers;
using System.Xml.Serialization;
using FTOS_GetUsersEnrollment.Models.Entities.HelpDesk;
using FTOS_GetUsersEnrollment.Models.Models.MailBoxModels;

namespace FTOS_GetUsersEnrollment.Service
{
    public partial class GetUsersEnrollmentService : ServiceBase
    {
        private int interval;
        private string serverPath;
        private Timer _timer;
        private Logger.Logger logger;

        public GetUsersEnrollmentService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            interval = int.Parse(ConfigurationManager.AppSettings["IntervalMinutes"]);
            serverPath = ConfigurationManager.AppSettings["ServerPath"];
            logger = new Logger.Logger();
            logger.LogPath = ConfigurationManager.AppSettings["LogPath"];

            _timer = new Timer(TimeSpan.FromMinutes(interval).TotalMilliseconds);
            _timer.Elapsed += new ElapsedEventHandler(OnTimerElapsedAsync);
            _timer.AutoReset = true;
            _timer.Start();

            EventLog.WriteEntry("HelpDesk e-mail reminder service with latest changes started successfully.");
            logger.AppendToLog("Service started at: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), true);
        }

        protected override void OnStop()
        {
            _timer.Stop();
            _timer.Dispose();
        }

        private void OnTimerElapsedAsync(object sender, ElapsedEventArgs e)
        {
            RestRequest requestForToken = new RestRequest(Method.POST);
            RestClient clientToken = new RestClient(ConfigurationManager.AppSettings["GetUsersEnrolmentTokenURL"]);
            var token = new TokenSend();

            token.Client_Id = ConfigurationManager.AppSettings["GetUsersEnrolmentID"];
            token.Username = ConfigurationManager.AppSettings["GetUsersEnrolmentUsername"];
            token.Password = ConfigurationManager.AppSettings["GetUsersEnrolmentPassword"];
            
            requestForToken.AddHeader("Content-Type", "application/json; charset=utf-8");
            requestForToken.RequestFormat = DataFormat.Json;
            requestForToken.AddJsonBody(JsonConvert.SerializeObject(token));

            var responseForToken = clientToken.Execute<Token>(requestForToken);
            
            GetUsersEnrollment(responseForToken);
            MailBox(responseForToken);
        }

        private void GetUsersEnrollment(IRestResponse<Token> token)
        {
            logger.AppendToLog("OnTimerElapsedAsync method first log ------------- 1 ------------- Method entry " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), true);
            
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var dateRaw = DateTime.UtcNow.Date;
                var date = dateRaw.ToString("yyyy-MM-dd");
                var nextDayDate = dateRaw.AddDays(1).ToString("yyyy-MM-dd");
                var finalData = new List<User>();
                var root = new RootSend();

                logger.AppendToLog("OnTimerElapsedAsync method second log ------------- 2 ------------- Befor using " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), true);

                using (HelpDesk_V2Entities context = new HelpDesk_V2Entities())
                {
                    logger.AppendToLog("OnTimerElapsedAsync method third log ------------- 3 ------------- After Using " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), true);

                    try
                    {
                        var tickets = context.View_FTOS_GetUsersEnrollment
                        .Select(x => x.ContractNumbers)
                        .ToList();

                        logger.AppendToLog("OnTimerElapsedAsync method fourth log ------------- 4 ------------- After taking data from view " + tickets.Count + " tickets " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), true);

                        //Request for data
                        RestClient clientData = new RestClient(ConfigurationManager.AppSettings["GetUsersEnrolmentURL"]);
                        RestRequest requestForData = new RestRequest(Method.POST);

                        requestForData.AddHeader("Content-Type", "application/json; charset=utf-8");
                        requestForData.RequestFormat = DataFormat.Json;

                        root.ApiInfo.Token = token.Data.access_token;
                        root.Request.ActionName = "FTOS_IB_GetUsersEnrollment";
                        root.Request.Data = $"{{requestId: \"FTOS_12345\", dateFrom: \"{date}\", dateTo: \"{nextDayDate}\" }}";

                        requestForData.AddJsonBody(JsonConvert.SerializeObject(root));

                        var responseForData = clientData.Execute<List<RootData>>(requestForData);

                        logger.AppendToLog("OnTimerElapsedAsync method sixth log ------------- 6 ------------- After get data status code: " + responseForData.StatusCode + " " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), true);

                        var data = JsonConvert.DeserializeObject<Root>(responseForData.Data[0].UIResult.Data);
                        //Request for data

                        logger.AppendToLog("OnTimerElapsedAsync method sixth log ------------- 6 ------------- After deserializing " + data.result.users.Count + " results " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), true);

                        foreach (var item in data.result.users)
                        {
                            var res = tickets.FirstOrDefault(x => x == item.enrolId);
                            if (res == null)
                            {
                                finalData.Add(item);
                            }
                        }

                        logger.AppendToLog("OnTimerElapsedAsync method seventh log ------------- 7 ------------- After filtering " + finalData.Count + " results " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), true);

                        if (finalData != null)
                        {
                            //Requesto to HelpDesk
                            RestClient clientHelpDesk = new RestClient(ConfigurationManager.AppSettings["SelfServiceURL"])
                            {
                                Authenticator = new HttpBasicAuthenticator(ConfigurationManager.AppSettings["SelfServiceUsername"], ConfigurationManager.AppSettings["SelfServicePassword"])
                            };
                            RestRequest requestToHelpDesk = new RestRequest(Method.POST);

                            requestToHelpDesk.AddHeader("Content-Type", "application/json; charset=utf-8");
                            requestToHelpDesk.RequestFormat = DataFormat.Json;
                            requestToHelpDesk.AddJsonBody(JsonConvert.SerializeObject(finalData));

                            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                            var some = clientHelpDesk.Execute(requestToHelpDesk);
                            //Requesto to HelpDesk

                            logger.AppendToLog("OnTimerElapsedAsync method eight log ------------- 8 ------------- After sending to selfservice status code: " + some.StatusCode + " " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), true);
                        }
                    }
                    catch (Exception ex)
                    {

                        logger.AppendToLog("OnTimerElapsedAsync method second try/catch log ------------- 2 " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), true);
                        logger.AppendToLog("OnTimerElapsedAsync method second try/catch log " + ex.Message + " " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), true);
                        logger.AppendToLog("OnTimerElapsedAsync method second try/catch log " + ex.InnerException + " " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), true);
                        logger.AppendToLog("OnTimerElapsedAsync method second try/catch log " + ex.InnerException.Message + " " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), true);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ex.Message + Environment.NewLine + ex.InnerException.Message, EventLogEntryType.Error);
                logger.AppendToLog(string.Format("Service crashed on {0} \n with exception: {1}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), ex.Message + Environment.NewLine + ex.InnerException != null ? ex.InnerException.Message : "", EventLogEntryType.Error), true);
            }
        }
        
        private void MailBox(IRestResponse<Token> token)
        {
            try
            {
                var dateRaw = DateTime.UtcNow.Date;
                var date = dateRaw.ToString("yyyy-MM-dd");
                var nextDayDate = dateRaw.AddDays(1).ToString("yyyy-MM-dd");
                var finalData = new List<MailBox>();
                var root = new RootSend();
                logger.AppendToLog(
                    string.Format("data11 data11 data11 {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        Environment.NewLine), false);

                using (HelpDesk_V2Entities context = new HelpDesk_V2Entities())
                {
                    logger.AppendToLog(
                        string.Format("weche sum tuk {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                            Environment.NewLine), false);


                    //var tickets = context.HelpDesk_data
                    //    .Join(context.HelpDesk_Application,
                    //        p => p.AppID,
                    //        pp => pp.Type_Name + pp.Type_ID,
                    //        (p, pp) => new { p, pp })
                    //    .Where(x => x.pp.HelpDeskTypeID == 94)
                    //    .AsEnumerable()
                    //    .Select(s => new MailBox
                    //    {
                    //        MessageId = SerializeXml.FromXML(s.p.Data).TC.ComplaintsRequests.RequestId
                    //    })

                    var tickets = context.View_FTOS_MailBox
                    .Select(x => x.MessageId)
                    .ToList();

                    logger.AppendToLog(
                        string.Format("data2 data2 data2 {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                            Environment.NewLine), false);
                    //Request for data
                    RestClient clientData =
                        new RestClient(ConfigurationManager.AppSettings["GetUsersEnrolmentURL"]);
                    RestRequest requestForData = new RestRequest(Method.POST);

                    requestForData.AddHeader("Content-Type", "application/json; charset=utf-8");
                    requestForData.RequestFormat = DataFormat.Json;

                    root.ApiInfo.Token = token.Data.access_token;
                    root.Request.ActionName = "FTOS_IB_Mailbox_OUT";
                    root.Request.Data =
                        $"{{requestId: \"FTOS_12345\", dateFrom: \"{date}\", dateTo: \"{nextDayDate}\" }}";

                    requestForData.AddJsonBody(JsonConvert.SerializeObject(root));

                    var responseForData = clientData.Execute<RootData>(requestForData);

                    var data = JsonConvert.DeserializeObject<MailBoxRoot>(responseForData.Data.UIResult.Data);
                    //Request for data

                    logger.AppendToLog(
                        string.Format("data3 data3 data3 {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                            Environment.NewLine), false);

                    foreach (var message in data.result.mailbox.messages)
                    {
                        var res = tickets.FirstOrDefault(x => x.MessageId == message.MessageId);

                        if (res == null)
                        {
                            finalData.Add(message);
                        }
                    }
                    
                    if (finalData.Count != 0)
                    {
                        //Request to HelpDesk
                        RestClient clientHelpDesk =
                            new RestClient(ConfigurationManager.AppSettings["HelpDeskEndpoint"])
                            {
                                Authenticator =
                                    new HttpBasicAuthenticator(ConfigurationManager.AppSettings["HelpDeskUsername"],
                                        ConfigurationManager.AppSettings["HelpDeskPassword"])
                            };
                        RestRequest requestToHelpDesk = new RestRequest(Method.POST);

                        requestToHelpDesk.AddHeader("Content-Type", "application/json; charset=utf-8");
                        requestToHelpDesk.RequestFormat = DataFormat.Json;
                        requestToHelpDesk.AddJsonBody(JsonConvert.SerializeObject(finalData));

                        var some = clientHelpDesk.Execute(requestToHelpDesk);
                        //Request to HelpDesk
                        logger.AppendToLog(
                            string.Format("data4 data4 data4 {0} {1}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                                some.StatusDescription, Environment.NewLine), false);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ex.Message + Environment.NewLine + ex.InnerException.Message,
                    EventLogEntryType.Error);
                logger.AppendToLog(
                    string.Format("Service crashed on {0} \n with exception: {1}",
                        DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        ex.Message + Environment.NewLine + ex.InnerException != null ? ex.InnerException.Message : "",
                        EventLogEntryType.Error), true);
            }
        }
    }

    public static class SerializeXml
    {
        private static string ToXml(APPLICATION a)
        {
            StringWriter sw = new StringWriter();

            try
            {
                XmlSerializer serializer = new XmlSerializer(a.GetType());
                serializer.Serialize(sw, a);
                return sw.ToString();
            }
            finally
            {
                sw.Close();
            }
        }

        public static APPLICATION FromXML(string s)
        {
            APPLICATION app = new APPLICATION();
            StringReader sr = new StringReader(s);

            try
            {
                XmlSerializer serializer = new XmlSerializer(app.GetType());
                app = (APPLICATION)serializer.Deserialize(sr);
                return app;
            }
            catch
            {
                throw;
            }
            finally
            {
                sr.Close();
            }
        }
    }
    
    [Serializable]
    public class APPLICATION
    {
        public Other_info Other_info;
        public Head_office_data Head_office_data;
        public private_inf private_inf;

        public TC TC;

        public bool IsHelpDesk = true;

        public APPLICATION()
        {
            this.Other_info = new Other_info();
            this.Head_office_data = new Head_office_data();
            this.private_inf = new private_inf();

            this.TC = new TC();
        }
    }
}