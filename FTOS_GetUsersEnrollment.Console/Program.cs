﻿using FTOS_GetUsersEnrollment.Data;
using FTOS_GetUsersEnrollment.Models.Entities.HelpDesk;
using FTOS_GetUsersEnrollment.Models.Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serialization.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace FTOS_GetUsersEnrollment.Console
{
    internal class Program
    {
        private static int interval;
        private static string serverPath;
        private static Timer _timer;
        private static Logger.Logger logger;

        static void Main(string[] args)
        {
            Setup();

            logger = new Logger.Logger();
            logger.LogPath = Path.Combine(@"C:\ProjectsTBI\FTOS");
            object obj = null;
            System.Timers.ElapsedEventArgs elapsedEventArgs = null;
            OnTimerElapsed(obj, elapsedEventArgs);

        }

        private static void Setup()
        {
            _timer = new Timer(TimeSpan.FromSeconds(10000).TotalMilliseconds);
            _timer.Elapsed += new ElapsedEventHandler(OnTimerElapsed);
            _timer.AutoReset = true;
        }

        private static void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var dateRaw = DateTime.UtcNow.Date;
            var date = dateRaw.ToString("yyyy-MM-dd");
            var nextDayDate = dateRaw.AddDays(1).ToString("yyyy-MM-dd");
            var finalData = new List<User>();
            var root = new RootSend();

            using (HelpDesk_V2Entities context = new HelpDesk_V2Entities())
            {

                var tickets = context.View_FTOS_GetUsersEnrollment
                    .Select(x => x.ContractNumbers)
                    .ToList();


                //Request for token
                RestClient clientToken = new RestClient("https://app-portal-mib-prod.azurewebsites.net/api/Authorize/GetToken");
                RestRequest requestForToken = new RestRequest(Method.POST);

                requestForToken.AddHeader("Content-Type", "application/json; charset=utf-8");
                requestForToken.RequestFormat = DataFormat.Json;

                var token = new TokenSend();

                token.Client_Id = "postman";
                token.Username = "tbi.prod.ib.api.call";
                token.Password = "bxz!zeh@zrk8YFA8jyk";

                requestForToken.AddJsonBody(JsonConvert.SerializeObject(token));

                var responseForToken = clientToken.Execute<Token>(requestForToken);
                //Request for token




                //Request for data
                RestClient clientData = new RestClient("https://app-portal-mib-prod.azurewebsites.net/Api/Openapi/CallAction");
                RestRequest requestForData = new RestRequest(Method.POST);

                requestForData.AddHeader("Content-Type", "application/json; charset=utf-8");
                requestForData.RequestFormat = DataFormat.Json;

                root.ApiInfo.Token = responseForToken.Data.access_token;
                root.Request.ActionName = "FTOS_IB_GetUsersEnrollment";
                root.Request.Data = $"{{requestId: \"FTOS_12345\", dateFrom: \"2022-09-28\", dateTo: \"2022-09-29\" }}";

                requestForData.AddJsonBody(JsonConvert.SerializeObject(root));

                var responseForData = clientData.Execute<List<RootData>>(requestForData);

                var data = JsonConvert.DeserializeObject<Root>(responseForData.Data[0].UIResult.Data);
                //Request for data


                foreach (var item in data.result.users)
                {
                    var res = tickets.FirstOrDefault(x => x == item.enrolId);
                    if (res == null)
                    {
                        finalData.Add(item);
                    }
                }


                if (finalData != null)
                {
                    //Requesto to HelpDesk
                    RestClient clientHelpDesk = new RestClient("https://intdev-appsrv.westeastbank.com:443/hdselfserviceapi/api/external/auto-online-banking-request")
                    {
                        Authenticator = new HttpBasicAuthenticator("Tb1b@nk_HD_0nline_b@nking", "Tb1b@nk_HD_l0g1n_b@nking")
                    };
                    RestRequest requestToHelpDesk = new RestRequest(Method.POST);

                    requestToHelpDesk.AddHeader("Content-Type", "application/json; charset=utf-8");
                    requestToHelpDesk.RequestFormat = DataFormat.Json;
                    requestToHelpDesk.AddJsonBody(JsonConvert.SerializeObject(finalData));

                    var some = clientHelpDesk.Execute(requestToHelpDesk);
                    //Requesto to HelpDesk
                }
            }
        }
    }
}
