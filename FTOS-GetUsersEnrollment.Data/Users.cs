//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FTOS_GetUsersEnrollment.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Users
    {
        public long UserID { get; set; }
        public string UserName { get; set; }
        public Nullable<int> BranchID { get; set; }
        public int Type { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Position { get; set; }
        public string MobilePhone { get; set; }
        public bool ActiveYn { get; set; }
        public int FailedAttempts { get; set; }
        public Nullable<System.DateTime> LastLiginDate { get; set; }
        public Nullable<System.DateTime> LastPassChangeDate { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public System.DateTime WhenCreated { get; set; }
        public int RecordType { get; set; }
        public string Department { get; set; }
        public string DirectManager { get; set; }
        public string IBAN { get; set; }
    }
}
