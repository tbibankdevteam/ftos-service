//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FTOS_GetUsersEnrollment.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Files
    {
        public long FileID { get; set; }
        public string CID { get; set; }
        public byte[] Data { get; set; }
        public string Filename { get; set; }
        public System.DateTime DataCreation { get; set; }
        public int StorageType { get; set; }
        public string StorageSubFolder { get; set; }
    }
}
