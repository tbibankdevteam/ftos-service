﻿using System;
using System.IO;

namespace FTOS_GetUsersEnrollment.Logger
{
    public class Logger
    {
        private string fileName;
        private FileStream fileStream;
        private string fullPath;

        public Logger()
        {

        }

        public string LogPath { get; set; }

        public void AppendToLog(string message, bool closeSection)
        {
            this.fileName = DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day;
            this.fullPath = LogPath + Path.DirectorySeparatorChar.ToString() + fileName + ".log";
            this.fileStream = new FileStream(this.fullPath, FileMode.Append);

            using (var sw = new StreamWriter(this.fileStream))
            {
                sw.WriteLine(message);

                if (closeSection)
                {
                    sw.WriteLine("-----END SECTION" + new string('-', 20));
                }
            }
        }
    }
}
