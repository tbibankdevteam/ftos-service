using FTOS_GetUsersEnrollment.Web.Helper.Entities.HelpDesk;
using System;

namespace FTOS_GetUsersEnrollment.Web.Helper.Entities
{
    [Serializable]
    public class APPLICATION
    {
        public Other_info Other_info;
        public Head_office_data Head_office_data;
        public private_inf private_inf;

        public TC TC;
        //OK TILLE HERE

        public bool IsHelpDesk = true;

        public APPLICATION()
        {
            this.Other_info = new Other_info();
            this.Head_office_data = new Head_office_data();
            this.private_inf = new private_inf();

            this.TC = new TC();
            //OK TILLE HERE
        }
    }
}
