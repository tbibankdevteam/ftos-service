using System;
using System.Collections.Generic;

namespace FTOS_GetUsersEnrollment.Web.Helper.Entities
{
    [Serializable]
    public class Other_info
    {
        public Other_info()
        {
        }

        public string Product = string.Empty;
        public string Amount = string.Empty;
        public string Currency = string.Empty;
        public string Period = string.Empty;
        public string collateral = string.Empty;
        public string Purpose = string.Empty;
        public string Client_AB = string.Empty;
        public string Client_other_bank = string.Empty;
        public string Products = string.Empty;
        public string Address_for_correspondence = string.Empty;
        public string Comment = string.Empty;
        public string Branch_notes = string.Empty;
        public string EmployeeBroughtClient = string.Empty;
        public string Sub_product = string.Empty;
        public string App_source = string.Empty;
        public string ApprovedForCreditCard = string.Empty;
        public string ClientWantCreditCard = string.Empty;
        public string AL_InvoiceAmount = string.Empty;

        public string BRManagerCommentsToBROfficer = string.Empty;
        public string BRManagerPreApproveComments = string.Empty;
        public string BRManagerFinalApproveComments = string.Empty;

        public string ROLegalCommentsToRO = string.Empty;
        public string ROLegalCommentsToBROfficer = string.Empty;

        public string HQAppraiserCommentsToHQA = string.Empty;
        public string HQAppraiserCommentsToBROfficer = string.Empty;

        public string CustomerCharacteristics = string.Empty;

        public string EvaluationCompany = string.Empty;

        public List<TrackComment> BranchNotesTrail = new List<TrackComment>();

    }
}
    