﻿using System;
using System.Collections.Generic;

namespace FTOS_GetUsersEnrollment.Web.Helper.Entities
{
    [Serializable]
    public class TrackComment
    {
        private string comment = string.Empty;
        //private string date;
        private DateTime dateCreation = DateTime.MinValue;
        //private string time;

        private string ntUser = string.Empty;
        private string userBranchID = string.Empty;
        private string userBranch = string.Empty;
        private int userRoleID = 0;
        //private string userRole;
        private string userFullName = string.Empty;

        private List<Attachment> attachments = new List<Attachment>();

        public string Comment { get { return comment; } set { comment = value; } }
        //public string Date { get { return date; } set { date = value; } }
        public DateTime DateCreation { get { return dateCreation; } set { dateCreation = value; } }
        //public string Time { get { return time; } set { time = value; } }

        public string NTUser { get { return ntUser; } set { ntUser = value; } }
        public string UserBranchID { get { return userBranchID; } set { userBranchID = value; } }
        public string UserBranch { get { return userBranch; } set { userBranch = value; } }
        public int UserRoleID { get { return userRoleID; } set { userRoleID = value; } }
        //public string UserRole { get { return userRole; } set { userRole = value; } }
        public string UserFullName { get { return userFullName; } set { userFullName = value; } }

        public List<Attachment> Attachments { get { return attachments; } set { attachments = value; } }
        public TrackComment()
        { }
    }
}
