﻿using System;

namespace FTOS_GetUsersEnrollment.Web.Helper.Entities
{
    [Serializable]
    public class Attachment
    {
        private string id = string.Empty;
        private string fileName = string.Empty;
        private string fileType = string.Empty;
        private string fileDescription = string.Empty;
        private DateTime date = DateTime.MinValue;
        private bool verified = true;
        private bool isArchive = false;

        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public string FileType
        {
            get { return fileType; }
            set { fileType = value; }
        }

        public string FileDescription
        {
            get { return fileDescription; }
            set { fileDescription = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public bool Verified
        {
            get { return verified; }
            set { verified = value; }
        }

        public bool IsArchive
        {
            get { return isArchive; }
            set { isArchive = value; }
        }

        public bool HasFile
        {
            get
            {
                return !string.IsNullOrEmpty(this.FileName);
            }
        }
    }
}
