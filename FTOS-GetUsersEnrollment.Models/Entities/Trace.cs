﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTOS_GetUsersEnrollment.Web.Helper.Entities
{
    [Serializable]
    public class Trace
    {
        public DateTime DateSending = DateTime.MinValue;
        public string Date = string.Empty;
        public string Time = string.Empty;
        public string NTUser = string.Empty;
        public string UserFullName = string.Empty;
        public int SelectedRoleID = 0;
        public string IP = string.Empty;
        public string Status = string.Empty;
        public string ModifiedBy = string.Empty;
    }
}
