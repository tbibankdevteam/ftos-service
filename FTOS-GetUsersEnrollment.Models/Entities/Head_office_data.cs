using System;
using System.Collections.Generic;
using System.Text;

namespace FTOS_GetUsersEnrollment.Web.Helper.Entities
{
    [Serializable]
    public class Head_office_data
    {
        public Head_office_data()
        {
        }

        public string approved_amount = string.Empty;
        public string approved_Period = string.Empty;
        public string accept_code = string.Empty;
        public string reject_code = string.Empty;

        public string Comment = string.Empty;
        public string Comment_new = string.Empty;
        public string Rating = string.Empty;


        public string Waiting_Reason_1 = string.Empty;
        public string Waiting_Reason_2 = string.Empty;
        public string Waiting_Reason_3 = string.Empty;
        public string Waiting_Reason_4 = string.Empty;
    }
}
