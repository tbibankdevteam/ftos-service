using System;
using System.Collections.Generic;

namespace FTOS_GetUsersEnrollment.Web.Helper.Entities
{
    [Serializable]
    public class private_inf
    {
        public private_inf()
        {
        }

        public string ID = string.Empty;
        public string Status = string.Empty;
        public string NTUser = string.Empty;
        public string UserFullName = string.Empty;
        public string BranchID = string.Empty;
        public string Branch = string.Empty;
        public int RoleID = 0;
        public string Role = string.Empty;
        public string AppType = string.Empty;
        public string guarantors_count = string.Empty;
        public DateTime DateSending = DateTime.MinValue;
        public string Date_of_sending = string.Empty;
        public string Time_of_sending = string.Empty;

        public string Priority = string.Empty;
        public string PriorityText = string.Empty;

        public List<Trace> StatusHistory = new List<Trace>();
        public Trace Trace = new Trace();

    }
}
