﻿using System;

namespace FTOS_GetUsersEnrollment.Models.Entities.HelpDesk
{
    [Serializable]
    public class MailBox
    {
        public string RecordId { get; set; }
        public string MessageId { get; set; }
        public string Subject { get; set; }
        public string Category { get; set; }
        public string Message { get; set; }
        public string Attachments { get; set; }
        public string Customer { get; set; }
        public string Date { get; set; }
        public string IBUser_FirstName { get; set; }
        public string IBUser_LastName { get; set; }
        public string IBUser_PIN { get; set; }
        public string aLookup5_name { get; set; }
        public string aLookup5_businessunitid { get; set; }
        public string ibUsers_businessUnitId_displayname { get; set; }
        public string aLookup6_userName { get; set; }
        public string aLookup6_systemuserid { get; set; }
        public string ibUsers_userId_displayname { get; set; }
        public string Customer_FirstName { get; set; }
        public string Customer_LastName { get; set; }
        public string Customer_PIN { get; set; }
        public string aLookup7_name { get; set; }
        public string aLookup7_businessunitid { get; set; }
        public string account_businessUnitId_displayname { get; set; }
        public string aLookup8_userName { get; set; }
        public string aLookup8_systemuserid { get; set; }
        public string account_userId_displayname { get; set; }
        public string aLookup1_name { get; set; }
        public string aLookup1_businessunitid { get; set; }
        public string base_businessUnitId_displayname { get; set; }
        public string aLookup2_category { get; set; }
        public string aLookup2_businessUnitId { get; set; }
        public string aLookup2_userId { get; set; }
        public string Category_displayname { get; set; }
        public string aLookup3_Name { get; set; }
        public string aLookup3_businessUnitId { get; set; }
        public string aLookup3_userId { get; set; }
        public string Customer_displayname { get; set; }
        public string aLookup4_userName { get; set; }
        public string aLookup4_systemuserid { get; set; }
        public string base_userId_displayname { get; set; }
    }
}
