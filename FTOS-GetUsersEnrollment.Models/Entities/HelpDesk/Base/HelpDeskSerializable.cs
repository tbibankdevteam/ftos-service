﻿namespace FTOS_GetUsersEnrollment.Web.Helper.Entities.HelpDesk.Base
{
    using System;

    [Serializable]
    public class HelpDeskSerializable
    {
        public string UCN = string.Empty;

        public string ContractNumbers = string.Empty;
    }
}
