﻿using System;
using System.Xml.Serialization;
using FTOS_GetUsersEnrollment.Models.Entities.HelpDesk;

namespace FTOS_GetUsersEnrollment.Web.Helper.Entities.HelpDesk
{
    [Serializable]
    public class TC
    {
        public int HelpDeskTypeID = 0;

        [XmlIgnore]
        public string UCN = string.Empty;

        [XmlIgnore]
        public string ContractNumbers = string.Empty;

        [XmlIgnore]
        public string Firstname = string.Empty;

        [XmlIgnore]
        public string Surname = string.Empty;

        [XmlIgnore]
        public string Lastname = string.Empty;

        //public AdvancedNewAccount AdvancedNewAccount = new AdvancedNewAccount();
        //public NewAccount NewAccount = new NewAccount();
        //public ChangeAccountRights ChangeAccountRights = new ChangeAccountRights();
        //public DeactivateAccount DeactivateAccount = new DeactivateAccount();
        //public ActivateAccount ActivateAccount = new ActivateAccount();
        //public BusinessCards BusinessCards = new BusinessCards();
        //public SoftwareIssue SoftwareIssue = new SoftwareIssue();
        //public HardwareIssue HardwareIssue = new HardwareIssue();
        //public CalculationPrepayment CalculationPrepayment = new CalculationPrepayment();
        //public CertificateRefinancing CertificateRefinancing = new CertificateRefinancing();
        //public ExpressCertificate ExpressCertificate = new ExpressCertificate();
        //public CertificateLackActiveContract CertificateLackActiveContract = new CertificateLackActiveContract();
        //public CertificateContractCompletion CertificateContractCompletion = new CertificateContractCompletion();
        //public CertificateLackPNB CertificateLackPNB = new CertificateLackPNB();
        //public CertificateCanceledContract CertificateCanceledContract = new CertificateCanceledContract();
        //public CertificateEndCreditCard CertificateEndCreditCard = new CertificateEndCreditCard();
        public ComplaintsRequests ComplaintsRequests = new ComplaintsRequests();
        //public EarlyPrepayment EarlyPrepayment = new EarlyPrepayment();
        //public ExtraPartialPrepayment ExtraPartialPrepayment = new ExtraPartialPrepayment();
        //public RequestFullRpayment RequestFullRpayment = new RequestFullRpayment();
        //public RequestPartialRepayment RequestPartialRepayment = new RequestPartialRepayment();
        //public RequestCreditCardAttachments RequestCreditCardAttachments = new RequestCreditCardAttachments();
        //public RequestCreditCardNoAttachments RequestCreditCardNoAttachments = new RequestCreditCardNoAttachments();
        //public RequestAdditionalCreditCardStatement RequestAdditionalCreditCardStatement = 
        //    new RequestAdditionalCreditCardStatement();
        //public RequestContractCopy RequestContractCopy = new RequestContractCopy();
        //public OtherRequest OtherRequest = new OtherRequest();
        //public OtherRequestCallCenter OtherRequestCallCenter = new OtherRequestCallCenter();
        //public OtherDocuments OtherDocuments = new OtherDocuments();
        //public RequestByEmail RequestByEmail = new RequestByEmail();
        //public RequestMobilePhone RequestMobilePhone = new RequestMobilePhone();
        //public RequestRoaming RequestRoaming = new RequestRoaming();
        //public RequestMobilePhoneProblem RequestMobilePhoneProblem = new RequestMobilePhoneProblem();
        //public CommunicationIssue CommunicationIssue = new CommunicationIssue();
        //public RelocationRequest RelocationRequest = new RelocationRequest();

        //public FullPrepayment FullPrepayment = new FullPrepayment();
        //public SignetRequest SignetRequest = new SignetRequest();
        //public RiskEvent RiskEvent = new RiskEvent();
        //public HeadWriteOffRequest HeadWriteOffRequest = new HeadWriteOffRequest();
        //public RequestRBASCheck RequestRBASCheck = new RequestRBASCheck();
        //public IdeaManager IdeaManager = new IdeaManager();
        //public PropertyIssue PropertyIssue = new PropertyIssue();
        //public DokoniProcessIssue DokoniProcessIssue = new DokoniProcessIssue();
        //public CarRequest CarRequest = new CarRequest();
        //public InternalAuditRequest InternalAuditRequest = new InternalAuditRequest();
        //public BOAContractCompletition BOAContractCompletition = new BOAContractCompletition();
        //public BOACanceledContract BOACanceledContract = new BOACanceledContract();
        //public BOALackActiveContract BOALackActiveContract = new BOALackActiveContract();
        //public BOACredits BOACredits = new BOACredits();
        //public BOACreditCards BOACreditCards = new BOACreditCards();
        //public BOAFullPrepayment BOAFullPrepayment = new BOAFullPrepayment();
        //public UniformRequest UniformRequest = new UniformRequest();
        //public TranslationRequest TranslationRequest = new TranslationRequest();
        //public UniqaRequest UniqaRequest = new UniqaRequest();
        //public OverpaidRefinancing OverpaidRefinancing = new OverpaidRefinancing();
        //public FullPrepaymentNew FullPrepaymentNew = new FullPrepaymentNew();
        //public ReferralRequest ReferralRequest = new ReferralRequest();
        //public MiddleOfficeRequest MiddleOfficeRequest = new MiddleOfficeRequest();
        //public DirectDebitRequest DirectDebitRequest = new DirectDebitRequest();
        //public SecurityWeaknessesReporting SecurityWeaknessesReporting = new SecurityWeaknessesReporting();
        //public SecurityIncidentRegistry SecurityIncidentRegistry = new SecurityIncidentRegistry();
        //public InternalAuditAssignment InternalAuditAssignment = new InternalAuditAssignment();
        //public ReassignUser ReassignUser = new ReassignUser();
        //public ReassignUserFromManager ReassignUserFromManager = new ReassignUserFromManager();
        //public CanceledContract CanceledContract = new CanceledContract();
        //public LoansRestructuring LoansRestructuring = new LoansRestructuring();
        //public AutoLoanRequest AutoLoanRequest = new AutoLoanRequest();
        //public RefundTicket RefundTicket = new RefundTicket();
        //public BankLoginRequest BankLoginRequest = new BankLoginRequest();
    }
}