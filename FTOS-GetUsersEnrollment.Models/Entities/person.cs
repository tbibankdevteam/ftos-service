using System;
using System.Collections.Generic;
using System.Text;

namespace FTOS_GetUsersEnrollment.Web.Helper.Entities
{
    [Serializable]
    public class person
    {
        private string name_Prefix = string.Empty;
        private string name = string.Empty;
        private string surname = string.Empty;
        private string familyName = string.Empty;
        private string familyNameANG = string.Empty;
        private string dateOfBirth = string.Empty;
        private string uCN = string.Empty;
        private string is_foreigner = string.Empty;
        private string id_No = string.Empty;
        private string issued_by = string.Empty;
        private string from = string.Empty;
        private string nationality = string.Empty;
        private string children_under_18 = string.Empty;
        private string work_phone = string.Empty;
        private string work_phone_Code = string.Empty;
        private string age = string.Empty;
        private string numberFamilyMember = string.Empty;

        public string Name_Prefix { get { return name_Prefix; } set { name_Prefix = value; } }
        public string Name { get { return name; } set { name = value; } }
        public string Surname { get { return surname; } set { surname = value; } }
        public string FamilyName { get { return familyName; } set { familyName = value; } }
        public string FamilyNameANG { get { return familyNameANG; } set { familyNameANG = value; } }
        public string DateOfBirth { get { return dateOfBirth; } set { dateOfBirth = value; } }
        public string UCN { get { return uCN; } set { uCN = value; } }
        public string Is_foreigner { get { return is_foreigner; } set { is_foreigner = value; } }
        public string ID_No { get { return id_No; } set { id_No = value; } }
        public string Issued_by { get { return issued_by; } set { issued_by = value; } }
        public string From { get { return from; } set { from = value; } }
        public string Nationality { get { return nationality; } set { nationality = value; } }
        public string Children_under_18 { get { return children_under_18; } set { children_under_18 = value; } }
        public string Work_phone { get { return work_phone; } set { work_phone = value; } }
        public string Work_phone_Code { get { return work_phone_Code; } set { work_phone_Code = value; } }
        public string Age { get { return age; } set { age = value; } }
        public string NumberFamilyMember { get { return numberFamilyMember; } set { numberFamilyMember = value; } }
    }
}
