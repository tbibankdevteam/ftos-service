﻿using System.Collections.Generic;

namespace FTOS_GetUsersEnrollment.Models.Models
{
    public class Result
    {
        public Header header { get; set; }
        public List<User> users { get; set; }
    }
}
