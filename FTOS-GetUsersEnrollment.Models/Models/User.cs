﻿namespace FTOS_GetUsersEnrollment.Models.Models
{
    public class User
    {
        public string enrolId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string PersonalIdentificationNumber { get; set; }
        public string DocumentNumber { get; set; }
        public string SystemUserName { get; set; }
        public string UserCode { get; set; }
        public string CoreBankingUniqueIdentifier { get; set; }
        //public bool GDPRConsent { get; set; }
        public string CountryName { get; set; }
    }
}
