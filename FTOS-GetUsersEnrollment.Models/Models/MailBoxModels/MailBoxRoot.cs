﻿using FTOS_GetUsersEnrollment.Models.Entities.HelpDesk;
using System.Collections.Generic;

namespace FTOS_GetUsersEnrollment.Models.Models.MailBoxModels
{
    public class MailBoxRoot
    {
        public MailBoxResult result { get; set; }
    }

    public class MailBoxResult
    {
        public Header header { get; set; }
        public MailBoxData mailbox { get; set; }
    }

    public class MailBoxData
    {
        public List<MailBox> messages { get; set; }
    }
}
