﻿namespace FTOS_GetUsersEnrollment.Models.Models
{
    public class TokenSend
    {
        public string Client_Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
