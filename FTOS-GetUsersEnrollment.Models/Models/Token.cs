﻿namespace FTOS_GetUsersEnrollment.Models.Models
{
    public class Token
    {
        public string error { get; set; }
        public string email { get; set; }
        public string access_token { get; set; }
        public string userName { get; set; }
        public double expires_in { get; set; }
    }
}
