﻿namespace FTOS_GetUsersEnrollment.Models.Models
{
    public class Request
    {
        public string ActionName { get; set; }

        public string Data { get; set; }
    }
}
