﻿namespace FTOS_GetUsersEnrollment.Models.Models
{
    public class RootSend
    {
        public ApiInfo ApiInfo { get; set; }
        public Request Request { get; set; }

        public RootSend()
        {
            this.ApiInfo = new ApiInfo();
            this.Request = new Request();
        }
    }
}
